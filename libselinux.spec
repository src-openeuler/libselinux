%global ruby_inc %(pkg-config --cflags ruby)
%global libsepol_version 3.5

Name:    libselinux
Version: 3.5
Release: 5
License: Public Domain
Summary: SELinux library and simple utilities
Url:     https://github.com/SELinuxProject/selinux/wiki
Source0: https://github.com/SELinuxProject/selinux/releases/download/%{version}/libselinux-%{version}.tar.gz

Patch0001: backport-libselinux-add-check-for-calloc-in-check_booleans.patch
Patch0002: backport-libselinux-utils-free-allocated-resources.patch
Patch0003: backport-libselinux-enable-usage-with-pedantic-UB-sanitizers.patch
Patch0004: backport-libselinux-reorder-calloc-3-arguments.patch
Patch0005: backport-libselinux-Fix-ordering-of-arguments-to-calloc.patch
Patch0006: backport-libselinux-use-reentrant-strtok_r-3.patch
Patch0007: backport-libselinux-fix-swig-bindings-for-4.3.0.patch
Patch0008: backport-libselinux-utils-selabel_digest-drop-unsupported-opt.patch
Patch0009: backport-libselinux-utils-selabel_digest-avoid-buffer-overflo.patch
Patch0010: backport-libselinux-free-data-on-selabel-open-failure.patch
Patch0011: backport-libselinux-avoid-logs-in-get_ordered_context_list-wi.patch
Patch0012: backport-libselinux-free-empty-scandir-3-result.patch
Patch0013: backport-libselinux-avoid-pointer-dereference-before-check.patch
Patch0014: backport-libselinux-set-free-d-data-to-NULL.patch
Patch0015: backport-libselinux-matchpathcon-RESOURCE_LEAK-Variable-con.patch
Patch0016: backport-libselinux-Close-old-selabel-handle-when-setting-a-n.patch

Patch9000: do-malloc-trim-after-load-policy.patch

BuildRequires: gcc python3-devel systemd swig pcre2-devel xz-devel
BuildRequires: ruby-devel libsepol-static python3-pip python3-setuptools python3-wheel

Requires:  libsepol >= %{libsepol_version} pcre2
Conflicts: filesystem < 3, selinux-policy-base < 3.13.1-138

Provides:  %{name}-utils = %{version}-%{release}
Obsoletes: %{name}-utils < %{version}-%{release}

%description
libselinux provides an interface to get and set process and file
security contexts and to obtain security policy decisions.

%package devel
Summary: Header files and libraries used to build SELinux
Requires: %{name} = %{version}-%{release}
Requires: libsepol-devel >= %{libsepol_version}

Provides:  %{name}-static = %{version}-%{release}
Obsoletes: %{name}-static < %{version}-%{release}

%description devel
libselinux provides an interface to get and set process and file
security contexts and to obtain security policy decisions.

%package -n python3-libselinux
Summary: SELinux python3 bindings for libselinux
Requires:  %{name} = %{version}-%{release}
Provides:  %{name}-python3 = %{version}-%{release}
Obsoletes: %{name}-python3 < %{version}-%{release}

%description -n python3-libselinux
The libselinux-python3 package contains the python bindings for developing
SELinux applications.

%package ruby
Summary: SELinux ruby bindings for libselinux
Requires: %{name} = %{version}-%{release}
Provides: ruby(selinux)

%description ruby
The libselinux-ruby package contains the ruby bindings for developing
SELinux applications.

%package_help

%prep
%autosetup -p 1 -n libselinux-%{version}

%build
export LDFLAGS="%{?__global_ldflags}"
export DISABLE_RPM="y"
export USE_PCRE2="y"

make clean
%make_build LIBDIR="%{_libdir}" CFLAGS="-g %{optflags}" swigify
%make_build LIBDIR="%{_libdir}" CFLAGS="-g %{optflags}" all
%make_build %{__python3} LIBDIR="%{_libdir}" CFLAGS="-g %{optflags}" pywrap
%make_build RUBYINC="%{ruby_inc}" SHLIBDIR="%{_libdir}" LIBDIR="%{_libdir}" LIBSEPOLA="%{_libdir}/libsepol.a" CFLAGS="-g %{optflags}" rubywrap

%install
mkdir -p %{buildroot}%{_tmpfilesdir}
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_includedir}
mkdir -p %{buildroot}%{_sbindir}

install -d -m 0755 %{buildroot}%{_rundir}/setrans
echo "d %{_rundir}/setrans 0755 root root" > %{buildroot}%{_tmpfilesdir}/libselinux.conf

make PYTHON=%{__python3} DESTDIR="%{buildroot}" LIBDIR="%{_libdir}" \
SHLIBDIR="%{_lib}" BINDIR="%{_bindir}" SBINDIR="%{_sbindir}" LIBSEPOLA="%{_libdir}/libsepol.a" install-pywrap

make DESTDIR="%{buildroot}" LIBDIR="%{_libdir}" SHLIBDIR="%{_libdir}" \
BINDIR="%{_bindir}" SBINDIR="%{_sbindir}" RUBYINSTALL=%{ruby_vendorarchdir} install install-rubywrap

rm -f %{buildroot}%{_sbindir}/{deftype,execcon,getenforcemode,getfilecon,getpidcon}
rm -f %{buildroot}%{_sbindir}/{mkdircon,policyvers,setfilecon,selinuxconfig,getseuser}
rm -f %{buildroot}%{_sbindir}/{compute_*,selinuxdisable,togglesebool,selinux_check_securetty_context}

mv %{buildroot}%{_sbindir}/getdefaultcon %{buildroot}%{_sbindir}/selinuxdefcon
mv %{buildroot}%{_sbindir}/getconlist %{buildroot}%{_sbindir}/selinuxconlist

%files
%license LICENSE
%{_libdir}/libselinux.so.*
%{_sbindir}/{selabel_lookup_best_match,selabel_partial_match,selinux_check_access}
%{_sbindir}/{avcstat,getenforce,getpidprevcon,getsebool,matchpathcon,sefcontext_compile,selinuxconlist}
%{_sbindir}/{selinuxdefcon,selinuxexeccon,selinuxenabled,setenforce,selabel_digest,selabel_lookup}
%{_sbindir}/{selabel_get_digests_all_partial_matches,validatetrans}
%dir %{_rundir}/setrans/
%{_tmpfilesdir}/libselinux.conf

%files devel
%{_libdir}/libselinux.a
%{_libdir}/libselinux.so
%{_libdir}/pkgconfig/libselinux.pc
%{_includedir}/selinux/

%files -n python3-libselinux
%{python3_sitearch}/selinux/
%{python3_sitearch}/selinux-%{version}*
%{python3_sitearch}/_selinux.*.so

%files ruby
%{ruby_vendorarchdir}/selinux.so

%files help
%{_mandir}/man3/*
%{_mandir}/man5/*
%{_mandir}/man8/*
%lang(ru) %{_mandir}/ru/man5/*
%lang(ru) %{_mandir}/ru/man8/*

%changelog
* Fri Mar 14 2025 yixiangzhike <yixiangzhike007@163.com> - 3.5-5
- backport upstream patches

* Wed Jan 22 2025 Funda Wang <fundawang@yeah.net> - 3.5-4
- add upstream patch fixing build with swig 4.3.0
- drop useless buildroot cleanup and ldconfig scriptlets

* Mon Mar 25 2024 fuanan <fuanan3@h-partners.com> - 3.5-3
- backport upstream patches

* Tue Jul 25 2023 luhuaxin <luhuaxin1@huawei.com> - 3.5-2
- add check for calloc in check_booleans
- fix python build issue

* Mon Jul 17 2023 zhangguangzhi <zhangguangzhi3@huawei.com> - 3.5-1
- update version to 3.5

* Mon Jan 30 2023 zhangguangzhi <zhangguangzhi3@huawei.com> - 3.4-1
- update version to 3.4

* Sun Oct 9 2022 lujie <lujie54@huawei.com> - 3.3-2
- backport upstream patches

* Wed Dec 8 2021 lujie <lujie42@huawei.com> - 3.3-1
- update libselinux-3.1 to libselinux-3.3

* Tue Nov 16 2021 luhuaxin <1539327763@qq.com> - 3.1-5
- backport upstream patches

* Mon Nov 15 2021 lujie <lujie42@huawei.com> - 3.1-4
- fix potential undefined shifts

* Wed Jul 2 2021 luhuaxin <1539327763@qq.com> - 3.1-3
- do malloc trim after load policy

* Tue Oct 27 2020 gaoyusong <gaoyusong1@huawei.com> - 3.1-2
- delete BuildRequires python2-devel 

* Fri Jul 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.1-1
- update to 3.1; delete python2-libselinux

* Tue Jun 23 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.9-3
- add missing _selniux.so

* Mon Jun 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.9-2
- use python distutils to install selinux python bindings

* Thu Sep 5 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.9-1
- Package init


